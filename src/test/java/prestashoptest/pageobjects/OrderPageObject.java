package prestashoptest.pageobjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import prestashoptest.SetupTeardown;
import prestashoptest.datatypes.CartAndOrderLine;
import prestashoptest.helpers.FloatsHelper;
import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Order page
 */
public class OrderPageObject extends PageObjectBase {

    private static final Selector RADIO_CONDITION_TO_APPROVE1 = new Selector(SelectBy.ID, "delivery_option_1");
    private static final Selector RADIO_CONDITION_TO_APPROVE2 = new Selector(SelectBy.ID, "delivery_option_2");
    private static final Selector FIELD_ALIAS = new Selector(SelectBy.ID, "field-alias");
    private static final Selector FIELD_COMPANY = new Selector(SelectBy.ID, "field-company");
    private static final Selector FIELD_VAT = new Selector(SelectBy.ID, "field-vat_number");
    private static final Selector FIELD_ADDRESS = new Selector(SelectBy.ID, "field-address1");
    private static final Selector FIELD_SUPP = new Selector(SelectBy.ID, "field-address2");
    private static final Selector FIELD_ZIP = new Selector(SelectBy.ID, "field-postcode");
    private static final Selector FIELD_CITY = new Selector(SelectBy.ID, "field-city");
    private static final Selector FIELD_COUNTRY = new Selector(SelectBy.XPATH, "//select[@id='field-id_country']");
    private static final Selector FIELD_PHONE = new Selector(SelectBy.ID, "field-phone");
    private static final Selector FIELD_DELIVERY_MESSAGE = new Selector(SelectBy.ID, "delivery_message");
    private static final Selector FIELD_EMAIL = new Selector(SelectBy.XPATH, "//div[contains(@id,'login-form')]//input[@id='field-email']");
    private static final Selector FIELD_PASSWORD = new Selector(SelectBy.XPATH, "//div[contains(@id,'login-form')]//input[@id='field-password']");
    private static final Selector CB_BILLING_ADDRESS = new Selector(SelectBy.ID, "use_same_address");
    private static final Selector CB_CONDITION_TO_APPROVE = new Selector(SelectBy.ID, "conditions_to_approve[terms-and-conditions]");
    private static final Selector BUTTON_TO_LOGIN_FORM = new Selector(SelectBy.XPATH, "//a[@data-link-action='show-login-form']");
    private static final Selector BUTTON_CONNECTION = new Selector(SelectBy.XPATH, "//*[@id='checkout-personal-information-step']//button[@data-link-action='sign-in']");
    private static final Selector BUTTON_ORDER = new Selector(SelectBy.XPATH, "//*[@id='payment-confirmation']//button[@type='submit']");
    private static final Selector ORDER_ITEM = new Selector(SelectBy.CLASS, "order-line");
    private static final Selector CONFIRM_ADDRESSES = new Selector(SelectBy.NAME, "confirm-addresses");
    private static final Selector CONFIRM_DELIVERY = new Selector(SelectBy.NAME, "confirmDeliveryOption");
    private static final Selector TOTAL_ORDER_PRICE = new Selector(SelectBy.XPATH, "//tr[contains(@class,'total-value')]/td[contains(text(), '€')]");
    private static final Selector ORDER_REFERENCE = new Selector(SelectBy.ID, "order-reference-value");

    public OrderPageObject() {
        super("order");
    }

    /**
     * Return the content of the cart.
     *
     * @return List of content
     */
    public List<CartAndOrderLine> getOrderContent() {
        final List<WebElement> items = getElements(ORDER_ITEM);
        final List<CartAndOrderLine> list = new ArrayList<>(items.size());
        for (final WebElement item : items) {
            final WebElement productElem = item.findElement(By.className("details"));
            final String product = cleanProductName(productElem.getText());
            final WebElement quantityElem = item.findElement(By.cssSelector("[class='col-xs-4 text-sm-center']"));
            final int quantity = Integer.parseInt(quantityElem.getText());
            final WebElement details = item.findElement(By.className("details"));
            final String dimension = extractDetails(details.getText(), "Dimension");
            final String size = extractDetails(details.getText(), "Size");
            final String color = extractDetails(details.getText(), "Color");
            final List<WebElement> customizationElem = item.findElements(By.className("product-customization-line"));
            final String customization = customizationElem.isEmpty() ? null : customizationElem.get(0).findElement(By.className("value")).getText().trim();
            final WebElement priceElem = item.findElement(By.xpath(".//div[contains(@class,'left') and contains(text(),'€')]"));
            final WebElement totalProductPriceElem = item.findElement(By.xpath(".//div[contains(@class,'right') and contains(text(),'€')]"));
            Float productPrice = null;
            Float totalProductPrice = null;
            try {
                productPrice = FloatsHelper.convertPriceStringToFloat(priceElem.getText());
                totalProductPrice = FloatsHelper.convertPriceStringToFloat(totalProductPriceElem.getText());
            } catch (final ParseException e) {
                Assertions.fail("The prices are not formatted correctly:" + e.getMessage());
            }
            list.add(new CartAndOrderLine(product, quantity, dimension, size, color, customization, productPrice, totalProductPrice));
        }
        return list;
    }

    /**
     * Fill the form with the specified values and initiate the account creation.
     *
     * @param alias The alias value to fill.
     * @param company The company value to fill.
     * @param vat The VAT value to fill.
     * @param address The address value to fill.
     * @param supp The supp value to fill.
     * @param zip The zip value to fill.
     * @param city The city value to fill.
     * @param phone The phone value to fill.
     */
        public void fillFormToOrder(final String alias,
                                final String company,
                                final String vat,
                                final String address,
                                final String supp,
                                final int zip,
                                final String city,
                                final String phone) {
        fillAlias(alias);
        fillCompany(company);
        fillVAT(vat);
        fillAddress(address);
        fillSupp(supp);
        fillZIP(zip);
        fillCity(city);
        fillPhone(phone);
    }

    /**
     * Fill the alias field.
     *
     * @param alias Alias to fill
     */
    public void fillAlias(final String alias) {
        fillFieldValue(FIELD_ALIAS, alias);
    }

    /**
     * Fill the company field.
     *
     * @param company Company to fill
     */
    public void fillCompany(final String company) {
        fillFieldValue(FIELD_COMPANY, company);
    }

    /**
     * Fill the VAT field.
     *
     * @param vat VAT to fill
     */
    public void fillVAT(final String vat) {
        fillFieldValue(FIELD_VAT, vat);
    }

    /**
     * Fill the address field.
     *
     * @param address Address to fill
     */
    public void fillAddress(final String address) {
        fillFieldValue(FIELD_ADDRESS, address);
    }

    /**
     * Fill the supp field.
     *
     * @param supp Address supplement to fill
     */
    public void fillSupp(final String supp) {
        fillFieldValue(FIELD_SUPP, supp);
    }

    /**
     * Fill the zip field.
     *
     * @param zip zip to fill
     */
    public void fillZIP(final int zip) {
        fillFieldValue(FIELD_ZIP, String.valueOf(zip));
    }

    /**
     * Fill the city field.
     *
     * @param city City to fill
     */
    public void fillCity(final String city) {
        fillFieldValue(FIELD_CITY, city);
    }

    /**
     * Fill the phone field.
     *
     * @param phone Phone to fill
     */
    public void fillPhone(final String phone) {
        fillFieldValue(FIELD_PHONE, phone);
    }

    /**
     * Choose the delivery.
     *
     * @param delivery Delivery to choose
     */
    public void chooseDelivery(final String delivery) {
        switch (delivery) {
            case "prestashop":
                if (!isCheckBoxSelected(RADIO_CONDITION_TO_APPROVE1)) {
                    clickElement(RADIO_CONDITION_TO_APPROVE1);
                }
                break;
            case "My carrier":
                if (!isCheckBoxSelected(RADIO_CONDITION_TO_APPROVE2)) {
                    clickElement(RADIO_CONDITION_TO_APPROVE2);
                }
                break;
            default:
                throw new IllegalStateException("Delivery unknown: " + delivery);
        }
    }

    /**
     * Click on the check box to use same address for billing as for delivery, if necessary.
     *
     * @param choice If the choice value is the same as the checkbox state, nothing happens, but if they are different, click in the checkbox
     */
    public void clickCheckBoxFacturationIfNecessary(final boolean choice) {
        if (choice ^ isCheckBoxSelected(CB_BILLING_ADDRESS)) {
            clickElement(CB_BILLING_ADDRESS);
        }
    }

    /**
     * Click on the check box to approve the terms of sales, if necessary.
     *
     * @param choice If the choice value is the same as the checkbox state, nothing happens, but if they are different, click in the checkbox
     */
    public void clickCheckBoxConditionIfNecessary(final boolean choice) {
        if (choice ^ isCheckBoxSelected(CB_CONDITION_TO_APPROVE)) {
            clickElement(CB_CONDITION_TO_APPROVE);
        }
    }

    /**
     * Fill delivery message.
     *
     * @param message Message to fill
     */
    public void fillDeliveryMessage(final String message) {
        fillFieldValue(FIELD_DELIVERY_MESSAGE, message);
    }

    /**
     * Submit address from.
     */
    public void submitAddressForm() {
        clickElement(CONFIRM_ADDRESSES);
    }

    /**
     * Submit delivery from.
     */
    public void submitDeliveryForm() {
        clickElement(CONFIRM_DELIVERY);
    }

    /**
     * Return the state of the order button.
     *
     * @return .
     */
    public boolean isOrderPossible() {
        return isButtonClickable(BUTTON_ORDER);
    }

    /**
     * Submit order.
     */
    public void submitOrder() {
        clickElement(BUTTON_ORDER);
    }

    /**
     * Click to access the login form.
     */
    public void goToLoginForm() {
        clickElement(BUTTON_TO_LOGIN_FORM);
    }

    /**
     * Fill the email field.
     *
     * @param email Email to fill out
     */
    public void fillEmail(final String email) {
        fillFieldValue(FIELD_EMAIL, email);
    }

    /**
     * Fill the password field.
     *
     * @param password Password to fill
     */
    public void fillPassword(final String password) {
        fillFieldValue(FIELD_PASSWORD, password);
    }

    /**
     * Click to connect.
     */
    public void submitConnection() {
        clickElement(BUTTON_CONNECTION);
    }

    /**
     * Choose the pay mode.
     *
     * @param paymode Pay mode to choose
     */
    public void choosePaymode(final String paymode) {
        final String dynamicXpath = "//span[contains(text(), '" + paymode + "')]/ancestor::div[contains(@id,'payment-option')]//input[@type='radio']";
        final Selector deliverySelector = new Selector(SelectBy.XPATH, dynamicXpath);
        clickElement(deliverySelector);
    }

    /**
     * Open the country drop-down menu and choose the country.
     *
     * @param country Country to choose
     */
    public void chooseCountry(final String country) {
        selectEntry(FIELD_COUNTRY, country);
    }

    /**
     * Get the total price after order.
     *
     * @return Total price
     */
    public float getTotalPrice() {
        final String totalPrice = getElementText(TOTAL_ORDER_PRICE);
        try {
            return FloatsHelper.convertStringToFloat(totalPrice);
        } catch (final ParseException e) {
            Assertions.fail("Price is improperly formatted: " + e.getMessage());
            return 0.0f; // cannot occur, necessary to compile
        }
    }

    /**
     * Extract a product name from a description.
     * <p>
     * For example:<br>
     * <code>cleanProductName("T-shirt imprimé colibri (Size : S - Color : White)")</code> -> <code>"T-shirt imprimé colibri"</code><br>
     *
     * @param description the description
     * @return the product name
     */
    private static String cleanProductName(final String description) {
        return description.replaceAll(" \\(.*$", "");
    }

    /**
     * Extract a detail from a description.
     * <p>
     * For example:<br>
     * <code>extractDetails("T-shirt imprimé colibri (Size : S - Color : White)", "Size")</code> -> <code>"S"</code><br>
     * <code>extractDetails("T-shirt imprimé colibri (Size : S - Color : White)", "Color")</code> -> <code>"White"</code><br>
     * <code>extractDetails("T-shirt imprimé colibri (Size : S - Color : White)", "Dimension")</code> -> <code>null</code>
     *
     * @param description the description
     * @param detail the detail to extract
     * @return the detail value, null if the detail is not found
     */
    private static String extractDetails(final String description, final String detail) {
        final Pattern pattern = Pattern.compile("(\\(|- )" + detail + " : (.+?)(\\)| -)");
        final Matcher matcher = pattern.matcher(description);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }

    /**
     * Get the reference of the order and record it to be deleted on the teardown.
     */
    public void setOrderToBeDeleted() {
        // On PrestaShop the reference is for example: "Référence de la commande : BELWVHUQU"
        //     * We need to reformat the text to extract only the reference 'BELWVHUQU' and use 'recordOrderToBeDeleted' to save it for the teardown process in 'deleteOrder'
        //     * We currently use the split() method to divide the string and select only the second element from the resulting array of strings.
        final String reference = getElementText(ORDER_REFERENCE);
        final String[] formattedReference = reference.split(": ");
        if (formattedReference.length != 2) {
            Assertions.fail("We couldn't retrieve the reference ID, so any order will be deleted.");
        }
        SetupTeardown.getLogger().log("INFO: The order reference is : " + formattedReference[1]);
        SetupTeardown.recordOrderToBeDeleted(formattedReference[1]);
    }

    /**
     * Verify that the address form has been correctly submitted.
     */
    public void assertValidAddressForm() {
        // we check that the delivery message field is visible
        waitElementIsVisible(FIELD_DELIVERY_MESSAGE);
    }
}