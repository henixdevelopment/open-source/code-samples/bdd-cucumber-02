package prestashoptest.pageobjects;

import java.time.LocalDate;

import org.openqa.selenium.TimeoutException;
import prestashoptest.SetupTeardown;
import prestashoptest.datatypes.Gender;
import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Personal information page
 */
public class IdentityPageObject extends PageObjectBase {

    private static final Selector RADIO_ID_MALE = new Selector(SelectBy.ID, "field-id_gender-1");
    private static final Selector RADIO_ID_FEMALE = new Selector(SelectBy.ID, "field-id_gender-2");
    private static final Selector FIELD_FIRST_NAME = new Selector(SelectBy.ID, "field-firstname");
    private static final Selector FIELD_LAST_NAME = new Selector(SelectBy.ID, "field-lastname");
    private static final Selector FIELD_EMAIL = new Selector(SelectBy.ID, "field-email");
    private static final Selector FIELD_PASSWORD = new Selector(SelectBy.ID, "field-password");
    private static final Selector FIELD_NEW_PASSWORD = new Selector(SelectBy.ID, "field-new_password");
    private static final Selector FIELD_BIRTHDAY = new Selector(SelectBy.ID, "field-birthday");
    private static final Selector CB_PARTNER_OFFERS = new Selector(SelectBy.NAME, "optin");
    private static final Selector CB_PRIVACY_POLICY = new Selector(SelectBy.NAME, "customer_privacy");
    private static final Selector CB_NEWSLETTER = new Selector(SelectBy.NAME, "newsletter");
    private static final Selector CB_GDPR = new Selector(SelectBy.NAME, "psgdpr");
    private static final Selector BUTTON_SUBMIT = new Selector(SelectBy.XPATH, "//*[@id='customer-form']//button[contains(@class,'submit')]");
    private static final Selector SUCCESSFUL_UPDATE_MESSAGE = new Selector(SelectBy.XPATH, "//article[contains(@class,'alert-success')]");

    public IdentityPageObject() {
        super("identity");
    }

    /**
     * Fill the gender field.<br>
     * If the gender is undefined, the field is untouched.
     *
     * @param gender Gender to fill
     */
    public void fillGender(final Gender gender) {
        if (gender.equals(Gender.UNDEFINED)) {
            return;
        }
        final Selector genderField = (gender.equals(Gender.MALE)) ? RADIO_ID_MALE
                : RADIO_ID_FEMALE;
        if (isCheckBoxSelected(genderField)) {
            return;
        }
        clickElement(genderField);
    }

    /**
     * Get the gender field value.
     *
     * @return Gender
     */
    public Gender getGender() {
        if (isCheckBoxSelected(RADIO_ID_MALE)) {
            return Gender.MALE;
        }
        if (isCheckBoxSelected(RADIO_ID_FEMALE)) {
            return Gender.FEMALE;
        }
        return Gender.UNDEFINED;
    }

    /**
     * Fill the first name field.
     *
     * @param firstName First name to fill
     */
    public void fillFirstName(final String firstName) {
        fillFieldValue(FIELD_FIRST_NAME, firstName);
    }

    /**
     * Get the first name field value.
     *
     * @return First name
     */
    public String getFirstName() {
        return getFieldValue(FIELD_FIRST_NAME);
    }

    /**
     * Fill the last name field.
     *
     * @param lastName Last name to fill
     */
    public void fillLastName(final String lastName) {
        fillFieldValue(FIELD_LAST_NAME, lastName);
    }

    /**
     * Get the last name field value.
     *
     * @return Last name
     */
    public String getLastName() {
        return getFieldValue(FIELD_LAST_NAME);
    }

    /**
     * Fill the email field.
     *
     * @param email Email to fill
     */
    public void fillEmail(final String email) {
        fillFieldValue(FIELD_EMAIL, email);
    }

    /**
     * Get the email field value.
     *
     * @return Email
     */
    public String getEmail() {
        return getFieldValue(FIELD_EMAIL);
    }

    /**
     * Fill the password field.
     *
     * @param password password to fill
     */
    public void fillPassword(final String password) {
        fillFieldValue(FIELD_PASSWORD, password);
    }

    /**
     * Fill the new password field.
     *
     * @param newPassword New password to fill
     */
    public void fillNewPassword(final String newPassword) {
        fillFieldValue(FIELD_NEW_PASSWORD, newPassword);
    }

    /**
     * Fill the birth date field.
     *
     * @param birthDate Birth date to fill
     */
    public void fillBirthDate(final LocalDate birthDate) {
        fillFieldValue(FIELD_BIRTHDAY, birthDate.format(getDateFormatter()));
    }

    /**
     * Get the birth date field value.
     *
     * @return Birth date
     */
    public LocalDate getBirthDate() {
        final String birthDateStr = getFieldValue(FIELD_BIRTHDAY);
        return LocalDate.parse(birthDateStr, getDateFormatter());
    }

    /**
     * Set partner offers by boolean.<br>
     * if true:  check if the checkbox isn't already selected or click on it.<br>
     * if false: check if the checkbox is selected then click on it to be unselected.
     */
    public void setPartnerOffersByBoolean(final boolean choice) {
        if (choice ^ doesAcceptPartnerOffers()) {
            clickElement(CB_PARTNER_OFFERS);
        }
    }

    /**
     * Get the partner approval field value.
     *
     * @return Partner approval
     */
    public boolean doesAcceptPartnerOffers() {
        return isCheckBoxSelected(CB_PARTNER_OFFERS);
    }

    /**
     * Approve the customer privacy policy.
     */
    public void acceptPrivacyPolicy() {
        clickElement(CB_PRIVACY_POLICY);
    }

    /**
     * Get the customer privacy field value.
     *
     * @return Customer privacy
     */
    public boolean doesAcceptPrivacyPolicy() {
        return isCheckBoxSelected(CB_PRIVACY_POLICY);
    }

    /**
     * Sign up for the newsletter.
     */
    public void setNewsletterByBoolean(final boolean choice) {
        if (choice ^ doesAcceptNewsletter()) {
            clickElement(CB_NEWSLETTER);
        }
    }

    /**
     * Get the newsletter field value.
     *
     * @return Newsletter
     */
    public boolean doesAcceptNewsletter() {
        return isCheckBoxSelected(CB_NEWSLETTER);
    }

    /**
     * Approve the GDPR policy.
     */
    public void acceptGdpr() {
        clickElement(CB_GDPR);
    }

    /**
     * Get the GDPR policy field value.
     *
     * @return GDPR policy
     */
    public boolean doesAcceptGdpr() {
        return isCheckBoxSelected(CB_GDPR);
    }

    /**
     * Initiate the account modification.
     */
    public void submitForm() {
        clickElement(BUTTON_SUBMIT);
    }

    /**
     * Fill identity-related fields with the specified values and initiate the account creation.
     *
     * @param gender              The gender to select.
     * @param firstName           The first name to fill.
     * @param lastName            The last name to fill.
     * @param email               The email address to fill.
     * @param password            The initial password to use.
     * @param birthDate           The birth date to fill.
     * @param acceptPartnerOffers boolean to indicate whether to accept partner offers.
     * @param acceptPrivacyPolicy boolean to indicate whether to accept the privacy policy.
     * @param acceptNewsletter    boolean to indicate whether to accept newsletters.
     * @param acceptGdpr          boolean to indicate whether to accept GDPR terms.
     */
    public void fillIdentityFields(final Gender gender,
                                   final String firstName,
                                   final String lastName,
                                   final String email,
                                   final String oldPass,
                                   final String password,
                                   final LocalDate birthDate,
                                   final boolean acceptPartnerOffers,
                                   final boolean acceptPrivacyPolicy,
                                   final boolean acceptNewsletter,
                                   final boolean acceptGdpr) {
        fillGender(gender);
        fillFirstName(firstName);
        fillLastName(lastName);
        fillEmail(email);
        fillPassword(oldPass);
        fillNewPassword(password);
        fillBirthDate(birthDate);
        setPartnerOffersByBoolean(acceptPartnerOffers);
        if (acceptPrivacyPolicy) acceptPrivacyPolicy();
        setNewsletterByBoolean(acceptNewsletter);
        if (acceptGdpr) acceptGdpr();
        submitForm();
        try {
            waitElementIsVisible(SUCCESSFUL_UPDATE_MESSAGE);
            SetupTeardown.replaceCustomerToBeDeleted(firstName, lastName, email, password);
            SetupTeardown.getLogger().log("INFO: Customer '" + email + "' successfully updated.");
        } catch (final TimeoutException e) {
            SetupTeardown.getLogger().log("INFO: Customer '" + email + "' could not be updated.");
        }
    }

    /**
     * Get success message from personal information update form.
     *
     * @return success message
     */
    public String getSuccessMessage() {
        waitElementIsVisible(SUCCESSFUL_UPDATE_MESSAGE);
        return getElementText(SUCCESSFUL_UPDATE_MESSAGE);
    }
}