package prestashoptest.pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import prestashoptest.seleniumtools.ComponentObjectBase;

/**
 * Header
 */
public class HeaderObject extends ComponentObjectBase {

    private static final String XPATH_MY_ACCOUNT = "//div[@id='_desktop_user_info']//a[contains(@href, 'my-account')]";
    private static final Selector LINK_MY_ACCOUNT = new Selector(SelectBy.XPATH, XPATH_MY_ACCOUNT);
    private static final Selector TEXT_DISPLAY_CUSTOMER = new Selector(SelectBy.XPATH, XPATH_MY_ACCOUNT + "/span");
    private static final Selector LINK_SIGN_OUT = new Selector(SelectBy.XPATH, "//*[@id='_desktop_user_info']//a[contains(@href, 'mylogout')]");
    private static final Selector LINK_SIGN_IN = new Selector(SelectBy.XPATH, "//*[@id='_desktop_user_info']//a[contains(@href, 'my-account')]");
    private static final Selector SEARCH_BAR = new Selector(SelectBy.XPATH, "//*[@id='search_widget']//input[@type='text']");

    /**
     * Test if the user is logged in.
     *
     * @return true is the user is logged in, false otherwise
     */
    public boolean isSignedIn() {
        final String title = getElementAttribute(LINK_MY_ACCOUNT, "title");
        if (title.equals("Voir mon compte client")) {
            return true;
        }
        if (title.equals("Identifiez-vous")) {
            return false;
        }
        throw new IllegalStateException("Unexpected title for (de)connection link: " + title);
    }

    /**
     * Return the displayed customer name.
     *
     * @return displayed customer name
     */
    public String getDisplayedCustomerName() {
        return getElementText(TEXT_DISPLAY_CUSTOMER);
    }

    /**
     * Sign out.
     */
    public void signOut() {
        clickElement(LINK_SIGN_OUT);
    }

    /**
     * Select a category.
     *
     * @param category Category to choose
     */
    public void selectCategory(final String category) {
        final Selector selector = new Selector(SelectBy.XPATH, "//li[@class='category']/a[@class='dropdown-item' and contains(@href,'" + category + "')]");
        clickElement(selector);
    }

    /**
     * Check the connection button is visible.
     */
    public void assertConnectionButtonIsVisible() {
        waitElementIsVisible(LINK_SIGN_IN);
    }

    /**
     * Search product with the search bar.
     *
     * @param product Product to look for
     */
    public void searchProduct(final String product) {
        waitElementIsVisible(SEARCH_BAR);
        fillFieldValue(SEARCH_BAR, product);
        final WebElement element = getElement(SEARCH_BAR);
        element.sendKeys(Keys.ENTER);
    }
}
