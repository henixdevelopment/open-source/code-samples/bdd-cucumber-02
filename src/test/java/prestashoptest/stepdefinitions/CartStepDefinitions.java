package prestashoptest.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import prestashoptest.SetupTeardown;
import prestashoptest.datatypes.CartAndOrderLine;
import prestashoptest.helpers.ApiHelper;
import prestashoptest.helpers.OrderContentVerification;
import prestashoptest.pageobjects.CartPageObject;

import java.text.DecimalFormat;
import java.util.List;

public class CartStepDefinitions {

    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ARRANGE-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Fill the cart.
     * 
     * @param cart cart content (see {@link OrderContentVerification#extractCartAndOrderLines})
     */
    @Given("The cart contains")
    public void setupCart(final DataTable cart) {
        final String userToBeDeletedEmail = SetupTeardown.getCustomerToBeDeleted().getEmail();
        if (userToBeDeletedEmail == null) {
            Assertions.fail("userToBeDeletedEmail is null, check if the user has been correctly recorded");
        }
        final List<CartAndOrderLine> cartLines = OrderContentVerification.extractCartAndOrderLines(cart.asMaps(String.class, String.class));
        ApiHelper.setupSessionCartForCustomer(cartLines, SetupTeardown.getCustomerToBeDeleted().getEmail());
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ARRANGE-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ACT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Update quantities in the cart.
     * 
     * @param cart cart content (see {@link OrderContentVerification#extractCartAndOrderLines})
     */
    @When("I update product quantities to")
    public void updateProductQuantities(final DataTable cart) {
        final CartPageObject cartPage = new CartPageObject();
        cartPage.assertIsCurrent();
        final List<CartAndOrderLine> expectedCartLines = OrderContentVerification.extractCartAndOrderLines(cart.asMaps(String.class, String.class));
        final List<String> productNames = cartPage.getProductNames(expectedCartLines);
        final List<Integer> productQuantities = cartPage.getQuantities(expectedCartLines);
        for (int i = 0; i < productNames.size(); i++) {
            final String productName = productNames.get(i);
            final int quantity = productQuantities.get(i).intValue();
            cartPage.updateQuantity(productName, quantity);
        }
    }

    /**
     * Remove product from the cart.
     * 
     * @param product
     */
    @When("I remove product {string}")
    public void removeProduct(final String product) {
        final CartPageObject cartPage = new CartPageObject();
        cartPage.assertIsCurrent();
        cartPage.removeProduct(product);
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ACT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ASSERT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓//

    /**
     * Verify that the cart data is equal to the given lines.
     * 
     * @param cart cart content (see {@link OrderContentVerification#extractCartAndOrderLines})
     */
    @Then("The cart should contain")
    public void assertCartContent(final DataTable cart) {
        final CartPageObject cartPage = new CartPageObject();
        cartPage.goTo();
        final List<CartAndOrderLine> expectedCartLines = OrderContentVerification.extractCartAndOrderLines(cart.asMaps(String.class, String.class));
        final List<CartAndOrderLine> effectiveCartLines = cartPage.getContent();
        OrderContentVerification.assertEquals(expectedCartLines, effectiveCartLines);
    }

    /**
     * Verify the total number of products and the total price.
     * 
     * @param totalQuantity
     * @param totalPrice
     */
    @Then("The total number of products should be {string} and the total price should be {string}")
    public void assertProductNumberAndTotalPrice(final String totalQuantity,
                                                 final String totalPrice) {
        final DecimalFormat decimalFormat = new DecimalFormat("0.00");
        final CartPageObject cartPage = new CartPageObject();
        Assertions.assertEquals(totalQuantity, String.valueOf(cartPage.getTotalProductQuantity()), "The displayed quantity is different from the expected quantity");
        Assertions.assertEquals(totalPrice, decimalFormat.format(cartPage.getTotalProductPrice()), "The displayed price is different from the expected price");
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ASSERT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑//
}